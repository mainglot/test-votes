<?php

class Test{

    public static function getTest(){
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM test');
       

        $i = 0;
        while ($row = $result->fetch()) {
            $test[$i]['id'] = $row['id'];
            $test[$i]['test_text'] = $row['test_text'];

            $i++;
        }

        return $test;
    }

    public static function getTestList()
    {

        $db = Db::getConnection();

        $testList = array();

        $result = $db->query('SELECT id, test_text FROM test');

        $i = 0;
        while ($row = $result->fetch()) {
            $testList[$i]['id'] = $row['id'];
            $testList[$i]['test_text'] = $row['test_text'];
            $i++;
        }

        return $TestList;
    }
    public static function getTestListAdmin(){

        $db = Db::getConnection();

        $testList = array();

        $result = $db->query('SELECT id, test_text FROM test');

        $i = 0;
        while ($row = $result->fetch()) {
            $testList[$i]['id'] = $row['id'];
            $testList[$i]['test_text'] = $row['test_text'];
            $i++;
        }

        return $testList;
    }
    
    public static function deleteTestById($id)
    {

        $db = Db::getConnection();

        $sql = 'DELETE FROM test WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateTestById($id, $test_text)
    {
        $db = Db::getConnection();

        $sql = "UPDATE test SET 
                    test_text = :test_text WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':test_text', $test_text, PDO::PARAM_STR);
        return $result->execute();
    }
    public static function getTestNameById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT test_text FROM test WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();
        $i = 0;
        while ($row = $result->fetch()) {
            $test_text = $row['test_text'];
            $i++;
        }

        return $test_text;
    }
    public static function getTestById($id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM test WHERE id = :id';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    public static function createTest($test_text){
        $db = Db::getConnection();

        $sql = 'INSERT INTO test (test_text) '
                . 'VALUES (:test_text)';

        $result = $db->prepare($sql);
        $result->bindParam(':test_text', $test_text, PDO::PARAM_STR);
        
        return $result->execute();
    }

}
