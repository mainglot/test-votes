<?php

class Result
{
    public static function insertResult($options){
        $db = Db::getConnection();
        
        
        
        $sql = 'INSERT INTO result '
                . '(test_id, result, date)'
                . 'VALUES (:test_id, :result, :date)';

        $result = $db->prepare($sql);
        $result->bindParam(':result', $options['result'], PDO::PARAM_STR);
        $result->bindParam(':date', $options['date'], PDO::PARAM_STR);
        $result->bindParam(':test_id', $options['test_id'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function getResultListAdmin(){

        $db = Db::getConnection();

        $resultList = array();

        $result = $db->query('SELECT * FROM result');

        $i = 0;
        while ($row = $result->fetch()) {
            $resultList[$i]['id'] = $row['id'];
            $resultList[$i]['test_id'] = Test::getTestNameById($row['test_id']);
            $resultList[$i]['date'] = $row['date'];
            $resultList[$i]['result'] = $row['result'];
            $i++;
        }

        return $resultList;
    }


}
