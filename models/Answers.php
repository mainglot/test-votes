<?php
//todo No base model for using DB
class Answers
{
    
    public static function getAnswersByTest($question_ids) {
        $answers = array();
        $db = Db::getConnection();

        $idsString = implode(',', $question_ids);
        $sql = "SELECT * FROM аnswers WHERE que_id IN ($idsString) ORDER BY RAND()";
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $i = 0;
        //todo bad way of fetching results, no encapsulation
        while ($row = $result->fetch()) {
            $answers[$i]['id'] = $row['id'];
            $answers[$i]['answer_text'] = $row['answer_text'];
            $answers[$i]['price'] = $row['price'];
            $answers[$i]['que_id'] = $row['que_id'];
            $answers[$i]['radio_flag'] = false;
            $i++;
        }
        


        return $answers; 
        
    }

    public static function getAnswersByIds($idsArray){
        $answers = array();
        $db = Db::getConnection();

        $idsString = implode(',', $idsArray);
        $sql = "SELECT * FROM аnswers WHERE id IN ($idsString)";
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $i = 0;
        while ($row = $result->fetch()) {
            $answers[$i]['id'] = $row['id'];
            $answers[$i]['answer_text'] = $row['answer_text'];
            $answers[$i]['price'] = $row['price'];
            $answers[$i]['que_id'] = $row['que_id'];
            $i++;
        }


        return $answers; 
    }

    public static function getAnswersById($id){
        $id = intval($id);

        if ($id) {                        
            $db = Db::getConnection();
            
            $result = $db->query('SELECT * FROM аnswers WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            return $result->fetch();
        }
    }

    public static function getAnswersList() {
        $db = Db::getConnection();
        
        $result = $db->query('SELECT * FROM аnswers ORDER BY id ASC');
        
        $answersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $answersList[$i]['id'] = $row['id'];
            $answersList[$i]['answer_text'] = $row['answer_text'];
            $answersList[$i]['price'] = $row['price'];
            $answersList[$i]['que_id'] = $row['que_id'];
            $i++;
        }

        return $answersList;
    }
    
    public static function deleteAnswersById($id) {
        $db = Db::getConnection();
        
        $sql = 'DELETE FROM аnswers WHERE id = :id';
        
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        
        return $result->execute() ;
        
    }
    public static function updateAnswersById($id, $options){
        
        $db = Db::getConnection();

      
        $sql = "UPDATE аnswers SET 
                answer_text = :answer_text, price = :price, que_id = :que_id
                WHERE id = :id";

      
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':answer_text', $options['answer_text'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':que_id', $options['que_id'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createAnswers($options){
        
        $db = Db::getConnection();

        
        $sql = 'INSERT INTO аnswers '
                . '(answer_text,  price, que_id)'
                . 'VALUES (:answer_text, :price, :que_id)';

        $result = $db->prepare($sql);
        $result->bindParam(':answer_text', $options['answer_text'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':que_id', $options['que_id'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

}