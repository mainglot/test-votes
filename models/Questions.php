<?php

class Questions
{
    public static function getQuestionsByTest($id) {
       
        $questions = array();
        $db = Db::getConnection();

        //$result = $db->query('SELECT * FROM questions q LEFT JOIN (SELECT * FROM аnswers ORDER BY RAND()) a ON (q.id = a.que_id) WHERE (q.test_id =1)');
        $result = $db->query('SELECT * FROM `questions`  WHERE test_id ='.$id);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while ($row = $result->fetch()) {
            $questions[$i]['id'] = $row['id'];
            $questions[$i]['is_checkbox'] = $row['is_checkbox'];
            $questions[$i]['question_text'] = $row['question_text'];
            $questions[$i]['test_id'] = $row['test_id'];
            $i++;
        }
        return $questions; 
    }

    public static function getQuestionsByIds($idsArray){
        $questions = array();
        $db = Db::getConnection();
        
        $idsString = implode(',', $idsArray);
        
        $sql = "SELECT * FROM questions  AND id IN ($idsString)";
        
        $result = $db->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        $i = 0;
        while ($row = $result->fetch()) {
            $questions[$i]['id'] = $row['id'];
            $questions[$i]['question_text'] = $row['question_text'];
            $questions[$i]['test_id'] = $row['test_id'];
            $i++;
        }
        return $questions; 
    }

    public static function getQuestionsById($id){
        $id = intval($id);

        if ($id) {                        
            $db = Db::getConnection();
            
            $result = $db->query('SELECT * FROM questions WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            return $result->fetch();
        }
    }

    public static function getQuestionsList() {
        $db = Db::getConnection();
        
        $result = $db->query('SELECT * FROM questions ORDER BY id ASC');
        
        $questionsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $questionsList[$i]['id'] = $row['id'];
            $questionsList[$i]['is_checkbox'] = $row['is_checkbox'];
            $questionsList[$i]['question_text'] = $row['question_text'];
            $questionsList[$i]['test_id'] = $row['test_id'];
            $i++;
        }

        return $questionsList;
    }

    public static function getQuestionsListAdmin(){
        $db = Db::getConnection();
        
        $result = $db->query('SELECT * FROM questions ORDER BY id ASC');
        
        $questionsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $questionsList[$i]['id'] = $row['id'];
            $questionsList[$i]['is_checkbox'] = $row['is_checkbox'];
            $questionsList[$i]['question_text'] = $row['question_text'];
            $questionsList[$i]['test_id'] = $row['test_id'];
            $i++;
        }

        return $questionsList;
    }
    
    public static function deleteQuestionsById($id) {
        $db = Db::getConnection();
        
        $sql = 'DELETE FROM questions WHERE id = :id';
        
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        
        return $result->execute() ;
        
    }
    public static function updateQuestionsById($id, $options){
        
        $db = Db::getConnection();

      
        $sql = "UPDATE questions SET 
                question_text = :question_text, is_checkbox = :is_checkbox, test_id = :test_id
                WHERE id = :id";

      
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':is_checkbox', $options['is_checkbox'], PDO::PARAM_INT);
        $result->bindParam(':question_text', $options['question_text'], PDO::PARAM_STR);
        $result->bindParam(':test_id', $options['test_id'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createQuestions($options){
        
        $db = Db::getConnection();

        
        $sql = 'INSERT INTO questions '
                . '(question_text, is_checkbox, test_id)'
                . 'VALUES (:question_text, :is_checkbox, :test_id)';

        $result = $db->prepare($sql);
        $result->bindParam(':is_checkbox', $options['is_checkbox'], PDO::PARAM_INT);
        $result->bindParam(':question_text', $options['question_text'], PDO::PARAM_STR);
        $result->bindParam(':test_id', $options['test_id'], PDO::PARAM_INT);
        if ($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

}