$(document).ready(function(){
	$('.to_test').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		var id = $(this).attr("data-id");
		$.ajax ({
			type: "POST",
			url: "siteController/getTest",
			data: {
					"id": id,
			},
			success: function(data) {
				$('.test_name').slideUp(200);
				
				$(".questions_wrapper").html(data);
				$('.inv').first().removeClass('inv').addClass('current');
			},
		});
})
// $(document).on('change', function(e){
// 	var input = $('.current .answer');
// 	if(input.is(e.target)){
// 		$('.current').removeClass('current');
// 		$('.question.inv').first().removeClass('inv').addClass('current');
// 	}
// })
$(document).on('click', function(e){
	var btn = $('.js-qc');
	if(btn.is(e.target) && $('.current input:checked').length){
		if($('.question.inv').length ){
			if($('.current input:checked').length){
				$('.current').removeClass('current').slideUp(200);
				$('.question.inv').first().removeClass('inv').addClass('current');
			}
		}else{
			var formdata = $('#test_form').serialize();
        $.ajax({
          type: 'POST',
          url: 'Result/getResult',
          data: formdata,
          success: function(data) {
						$('.current').removeClass('current').slideUp(200);
						$(".questions_wrapper").html(data);
          },
			});
		}
	}

})
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});
