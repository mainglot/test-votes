<?php

return array(

    'user/register' => 'user/register',
    'user/login' => 'user/login',
    'user/logout' => 'user/logout',
    
    'admin/answers/create' => 'adminAnswers/create',
    'admin/answers/update/([0-9]+)' => 'adminAnswers/update/$1',
    'admin/answers/delete/([0-9]+)' => 'adminAnswers/delete/$1',
    'admin/answers' => 'adminAnswers/index',
    
    'admin/questions/create' => 'adminQuestions/create',
    'admin/questions/update/([0-9]+)' => 'adminQuestions/update/$1',
    'admin/questions/delete/([0-9]+)' => 'adminQuestions/delete/$1',
    'admin/questions' => 'adminQuestions/index',

    'admin/test/create' => 'adminTest/create',
    'admin/test/update/([0-9]+)' => 'adminTest/update/$1',
    'admin/test/delete/([0-9]+)' => 'adminTest/delete/$1',
    'admin/test' => 'adminTest/index',

    'admin/result' => 'adminResult/index',
    
    'admin' => 'admin/index',

    'cabinet/edit' => 'cabinet/edit',
    'cabinet' => 'cabinet/index',
    
    '' => 'site/index' 
    
);
