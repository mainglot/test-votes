-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 05 2018 г., 12:22
-- Версия сервера: 5.5.50
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL,
  `test_id` int(11) DEFAULT NULL,
  `is_checkbox` int(11) DEFAULT NULL,
  `question_text` text
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `test_id`, `is_checkbox`, `question_text`) VALUES
(1, 1, NULL, 'Какой тип данных отсутствует в PHP?'),
(2, 1, 1, 'Какой из вариантов даст результат true в условно операторе?'),
(4, 1, 1, 'Как обратиться к методу родительского класса внутри метода?'),
(5, 1, 1, 'Через какой модуль PHP 7 Вы чаще всего подключаетесь к БД?'),
(6, 1, 1, 'Какой должна быть переменная $func, чтобы результат выполнения  echo array_sum(array_map($func, range(2, 6));  был 10?'),
(7, 1, NULL, 'Как узнать сколько символов в строке?'),
(8, 1, NULL, 'Какой конструкции нет в PHP 7?'),
(9, 3, NULL, '12'),
(10, 3, NULL, '13'),
(11, 3, 1, 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL,
  `test_id` int(11) DEFAULT NULL,
  `date` text,
  `result` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `result`
--

INSERT INTO `result` (`id`, `test_id`, `date`, `result`) VALUES
(1, 1, '1533451709', '17'),
(2, 1, '1533451800', '17'),
(3, 1, '1533451858', '17'),
(4, 1, '1533451902', '17'),
(5, 3, '1533455019', '-6'),
(6, 1, '1533455664', '13');

-- --------------------------------------------------------

--
-- Структура таблицы `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL,
  `test_text` text
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test`
--

INSERT INTO `test` (`id`, `test_text`) VALUES
(1, 'Первый тест'),
(3, 'пробный 2й тест 2');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL DEFAULT '0',
  `role` varchar(50) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Valik', 'cardinaliv94@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `аnswers`
--

CREATE TABLE IF NOT EXISTS `аnswers` (
  `id` int(11) NOT NULL,
  `que_id` int(11) DEFAULT NULL,
  `answer_text` text,
  `price` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `аnswers`
--

INSERT INTO `аnswers` (`id`, `que_id`, `answer_text`, `price`) VALUES
(1, 1, 'integer', '-2'),
(2, 1, 'resource', '-2'),
(3, 1, 'function', '+1'),
(4, 1, 'object', '-2'),
(5, 1, 'array', '-2'),
(6, 2, 'isset(null) ', '-2'),
(7, 2, '(function(){ return is_numeric(true); })() ', '-2'),
(8, 2, '42', '+1'),
(9, 2, 'empty(7 - 7)', '+2'),
(10, 2, '42 === “42”', '-2'),
(12, 4, '$this->parentMethod()', '+1'),
(13, 4, 'parent::parentMethod()', '+2'),
(14, 4, 'self::$parentMethod', '-2'),
(15, 4, '$obj->parentMethod()', '-2'),
(16, 5, 'mysqli', '+1'),
(17, 5, 'pdo', '+2'),
(18, 5, 'mssql', '-2'),
(19, 5, 'odbc', '+1'),
(20, 5, 'mysql', '-2'),
(21, 6, '$func = ‘intval’;', '-2'),
(22, 6, '$func = ‘trim’;', '-2'),
(23, 6, '$func = function(){ return 3 };', '-2'),
(24, 6, '$func = function($v){ return $v / 2; } ', '+1'),
(25, 6, '$func = function($v){ return intval($v/ 2); } ', '+2'),
(26, 7, 'length(‘Мышка Долли приготовила пирог’);', '-2'),
(27, 7, 'string_length(‘У Пеппи все чулки длинные’);', '-2'),
(28, 7, 'strlen(‘К нам приехал цирк’);', '-1'),
(29, 7, 'mb_strlen(‘Искин нашел следы людей’); ', '+2'),
(30, 8, '?? ', '-1'),
(31, 8, '?:', '-1'),
(32, 8, '>= ', '-2'),
(33, 8, '==', '-2'),
(34, 8, '?!', '+2'),
(35, 9, '1', '+1'),
(36, 9, '2', '-6'),
(37, 10, '1', '+6'),
(38, 10, '2', '-1');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `аnswers`
--
ALTER TABLE `аnswers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `аnswers`
--
ALTER TABLE `аnswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
