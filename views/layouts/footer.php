    <div class="page-buffer"></div>
</div>

<footer id="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="left-sidebar">
                    </div>
                </div>
            
                <div class="col-sm-6">
                    <div class="right-sidebar">
                        <div class="column-maps">
                        </div>
                    </div>
                </div>        
            </div>
        </div>        
    </div>
    
</footer>



<script src="/template/js/jquery.js"></script>
<script src="/template/js/jquery.cycle2.min.js"></script>
<script src="/template/js/jquery.cycle2.carousel.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>
<script>
    $(document).ready(function(){
        
        $(".add-to-cart").click(function(){
            var id = $(this).attr("data-id");
            $.post("/cart/addAjax/"+id,{}, function(data) {
                $("#cart-count").html(data);
            });
            return false;
        });
    });
</script>

</body>
</html>