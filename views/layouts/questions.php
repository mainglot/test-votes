<?php if(isset($questions) && !empty($questions)):?>

    <form id="test_form" method="POST">
        <?php $i = 1;?>
        <?php foreach($questions as $question) : ?>
            <div class="col-md-12 col-sm-6 col-xs-12 question inv" id="<?=$i;?>">
                <h2>Вопрос: <?=$i." из ".count($questions);?> </h2>
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <i class="icon-calendar"></i>
                        <h3 class="panel-title"><?= $question["question_text"];?></h3>
                    </div>
                    <?php if($question['is_checkbox'] == 1): ?>
                        <div class="panel-body">
                            <?php foreach($answers as $answer) : ?>
                                <?php if($answer['que_id'] == $question['id']) : ?>
                                    <label class="checkbox">
                                        <input  class="answer" name="ch_<?=$answer['id'];?>" type="checkbox" value="<?= $answer["id"];?>" data-id="<?= $answer["id"];?>"><?= $answer["answer_text"];?>
                                    </label>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php else : ?>
                        <div class="panel-body">
                            <?php foreach($answers as $answer) : ?>
                                <?php if($answer['que_id'] == $question['id']) : ?>
                                    <label class="radio">
                                        <input  class="answer" name="<?=$question['id'];?>" type="radio" value="<?= $answer["id"];?>" data-id="<?= $answer["id"];?>"><?= $answer["answer_text"];?>
                                    </label>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php $i++;?>
        <?php endforeach; ?>
        <input type="hidden" name="test_id" value="<?=$questions[0]['test_id'];?>">
    </form>

    <button class="btn btn-success  js-qc">></button>
<?php else: ?>
    <p>Нету вопросов для вас :(</p>
<?php endif; ?>