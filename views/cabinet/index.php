<?php include ROOT . '/views/layouts/header.php'; ?>
<section>
    <div class="container">
        <h1>Личный кабинет</h1>
        <h3>Вы вошли как: <?php echo $user['name'];?></h3>
        <ul>
            <li><a href="/cabinet/edit/">Редактировать данные</a></li>
            <li><a href="/cabinet/history/">Ваши покупки</a></li>            
        </ul>
    </div>
</section>


<?php include ROOT . '/views/layouts/footer.php'; ?>


