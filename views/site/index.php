<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <?php foreach($tests as $test) : ?>
                <div class="col-xs-4 test_name">
                    <a href="#" class="to_test" data-id="<?= $test['id'];?>"><?=$test['test_text'];?> </a>
                </div>
            <?php endforeach; ?>   
            <div class="questions_wrapper">
               
            </div>
        </div>
    </div>


</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>