<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li class="active">Просмотр результатов</li>
                </ol>
            </div>

            <h4>Список результатов</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>Пройденый тест</th>
                    <th>Дата проходжения</th>
                    <th>Результат прохождения</th>
                </tr>
                <?php foreach ($resultList as $result): ?>
                    <tr>
                        <td><?php echo $result['test_id']; ?></td>
                        <td><?php echo date(' H:i:s Y-m-d', $result['date']); ?></td>
                        <td><?php echo $result['result']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

