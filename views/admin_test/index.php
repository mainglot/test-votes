<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li class="active">Управление тестами</li>
                </ol>
            </div>

            <a href="/admin/test/create" class="btn btn-default back"><i class="fa fa-plus"></i> Добавить тест</a>
            
            <h4>Список тестов</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID теста</th>
                    <th>тест</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($testList as $test): ?>
                    <tr>
                        <td><?php echo $test['id']; ?></td>
                        <td><?php echo $test['test_text']; ?></td>
                        <td><a href="/admin/test/update/<?php echo $test['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/admin/test/delete/<?php echo $test['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

