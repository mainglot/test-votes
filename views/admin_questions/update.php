<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li><a href="/admin/questions">Управление вопросами</a></li>
                    <li class="active">Добавить вопрос</li>
                </ol>
            </div>


            <h4>Редактировать вопрос "<?php echo $questions['question_text']; ?>"</h4>

            <br/>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post">

                        <p>Название</p>
                        <input type="text" name="question_text" placeholder="" value="<?php echo $questions['question_text']; ?>">
                        <p>Несколько вариантов</p>
                        <input type="text" name="is_checkbox" placeholder="" value="<?php echo $questions['is_checkbox']; ?>">

                        <p>Тест</p>
                        <select name="test_id">
                            <?php if (is_array($testList)): ?>
                                <?php foreach ($testList as $test): ?>
                                    <option value="<?php echo $test['id']; ?>" 
                                        <?php if ($questions['test_id'] == $test['id']) echo ' selected="selected"'; ?>>
                                        <?php echo $test['test_text']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>

                        <br><br>
                        
                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

