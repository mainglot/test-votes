<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li class="active">Управление вопросами</li>
                </ol>
            </div>

            <a href="/admin/questions/create" class="btn btn-default back"><i class="fa fa-plus"></i> Добавить вопрос</a>
            
            <h4>Список вопросов</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID вопроса</th>
                    <th>вопрос</th>
                    <th>несколько вариантов</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($questionsList as $questions): ?>
                    <tr>
                        <td><?php echo $questions['id']; ?></td>
                        <td><?php echo $questions['question_text']; ?></td>
                        <td><?php if($questions['is_checkbox'] == 1){ echo "Да"; }else{ echo "Нет"; } ?></td>
                        <td><a href="/admin/questions/update/<?php echo $questions['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/admin/questions/delete/<?php echo $questions['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

