<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li><a href="/admin/questions">Управление вопросами</a></li>
                    <li class="active">Добавить вопрос</li>
                </ol>
            </div>


            <h4>Добавить новый вопрос</h4>

            <br/>

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post">

                        <p>Вопрос</p>
                        <input type="text" name="question_text" placeholder="" value="">
                        <p>Несколько вариантов</p>
                        <input type="text" name="is_checkbox" placeholder="" value="">

                        <p>Tест</p>
                        <select name="test_id">
                            <?php if (is_array($testList)): ?>
                                <?php foreach ($testList as $test): ?>
                                    <option value="<?php echo $test['id']; ?>" >
                                        <?php echo $test['test_text']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>

                        <br><br>

                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                    </form>
                </div>
            </div>


        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

