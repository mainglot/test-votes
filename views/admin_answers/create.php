<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li><a href="/admin/answers">Управление вариантами ответов</a></li>
                    <li class="active">Редактировать вариант</li>
                </ol>
            </div>


            <h4>Добавить новый вариант ответа</h4>

            <br/>

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post">

                        <p>Вариант ответа</p>
                        <input type="text" name="answer_text" placeholder="" value="">

                        <p>балы +/-</p>
                        <input type="text" name="price" placeholder="" value="">

                        <p>Вопрос</p>
                        <select name="que_id">
                            <?php if (is_array($questionsList)): ?>
                                <?php foreach ($questionsList as $question): ?>
                                    <option value="<?php echo $question['id']; ?>" >
                                        <?php echo $question['question_text']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>

                        <br/><br/>

                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">

                        <br/><br/>

                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

