<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li class="active">Управление вариантами ответов</li>
                </ol>
            </div>

            <a href="/admin/answers/create" class="btn btn-default back"><i class="fa fa-plus"></i> Добавить вариант</a>
            
            <h4>Список вариантов</h4>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID варианта</th>
                    <th>Вариант</th>
                    <th>балы +/-</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($answersList as $answers): ?>
                    <tr>
                        <td><?php echo $answers['id']; ?></td>
                        <td><?php echo $answers['answer_text']; ?></td>
                        <td><?php echo $answers['price']; ?></td>  
                        <td><a href="/admin/answers/update/<?php echo $answers['id']; ?>" title="Редактировать"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/admin/answers/delete/<?php echo $answers['id']; ?>" title="Удалить"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

