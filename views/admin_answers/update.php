<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Админпанель</a></li>
                    <li><a href="/admin/answers">Управление вариантами ответа</a></li>
                    <li class="active">Редактировать товар</li>
                </ol>
            </div>


            <h4>Редактировать вариант №<?php echo $id; ?></h4>

            <br/>

            <div class="col-lg-4">
                <div class="login-form">
                    <form action="#" method="post">

                        <p>Вариант ответа</p>
                        <input type="text" name="answer_text" placeholder="" value="<?php echo $answers['answer_text']; ?>">

                        <p>балы +/-</p>
                        <input type="text" name="price" placeholder="" value="<?php echo $answers['price']; ?>">

                        <p>Вопрос</p>
                        <select name="que_id">
                            <?php if (is_array($questionsList)): ?>
                                <?php foreach ($questionsList as $question): ?>
                                    <option value="<?php echo $question['id']; ?>" 
                                        <?php if ($answers['que_id'] == $question['id']) echo ' selected="selected"'; ?>>
                                        <?php echo $question['question_text']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        
                        <br/><br/>

                        
                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                        
                        <br/><br/>
                        
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

