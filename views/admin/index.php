<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
                                   
            <p>Вам доступны такие возможности:</p>
            
            <br/>
            
            <ul>
                <li><a href="/admin/answers">Управление ответами</a></li>
                <li><a href="/admin/questions">Управление вопросами</a></li>
                <li><a href="/admin/test">Управление тестами</a></li>
                <li><a href="/admin/result">Просмотр результатов</a></li>
            </ul>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

