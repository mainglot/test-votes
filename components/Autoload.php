<?php

function __autoload($class_name){
    $array_paths = array(
        '/models/',
        '/components/'
    );
    
    foreach ($array_paths as $path){
        //todo what if we don`t have this constant?
        $path = ROOT . $path . $class_name . '.php';
        if(is_file($path)){
            //todo here must be required_once
            include_once $path;
        }
    }
}