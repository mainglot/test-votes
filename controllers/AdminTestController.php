<?php 

class AdminTestController extends AdminBase{
    
    public function actionIndex(){
        self::checkAdmin();
        
        $testList = Test::getTestListAdmin();
        
        require_once (ROOT. '/views/admin_test/index.php');
        return true;
    }
    
    public function actionCreate()
    {

        self::checkAdmin();

        if (isset($_POST['submit'])) {
            $test_text = $_POST['test_text'];

            // Флаг ошибок в форме
            $errors = false;

            if (!isset($test_text) || empty($test_text)) {
                $errors[] = 'Заполните поля';
            }


            if ($errors == false) {
                Test::createTest($test_text);

                header("Location: /admin/test");
            }
        }

        require_once(ROOT . '/views/admin_test/create.php');
        return true;
    }

    public function actionUpdate($id){

        self::checkAdmin();

        $test = Test::getTestById($id);

        if (isset($_POST['submit'])) {
            $test_text = $_POST['test_text'];

            Test::updateTestById($id, $test_text);

            header("Location: /admin/test");
        }

        require_once(ROOT . '/views/admin_test/update.php');
        return true;
    }

    public function actionDelete($id){

        self::checkAdmin();

        if (isset($_POST['submit'])) {
            Test::deleteTestById($id);

            header("Location: /admin/test");
        }

        require_once(ROOT . '/views/admin_test/delete.php');
        return true;
    }
}

