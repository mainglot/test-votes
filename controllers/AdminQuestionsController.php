<?php

class AdminQuestionsController extends AdminBase{
    
    public function actionIndex(){
        self::checkAdmin();
        
        $questionsList = Questions::getQuestionsList();
        
        require_once (ROOT. '/views/admin_questions/index.php');
        return true;
    }
    
    public function actionCreate(){
        self::checkAdmin();
        
        $testList = Test::getTestListAdmin();
        
        if(isset($_POST['submit'])){
            $options['is_checkbox'] = $_POST['is_checkbox'];
            $options['question_text'] = $_POST['question_text'];
            $options['test_id'] = $_POST['test_id'];
           
            
            $errors = false;
            
            if(!isset($options['question_text']) || empty($options['question_text'])){
                $errors[] = 'Заполните поле question_text';
            }
           
            if(!isset($options['test_id']) || empty($options['test_id'])){
                $errors[] = 'Выберите тест';
            }

            
            if($errors == false){
                $id = Questions::createQuestions($options);
                
                header("Location: /admin/questions");
            }
            
        }
        require_once(ROOT . '/views/admin_questions/create.php');
        return true;
        
    }
    
    public function actionUpdate($id){
       
        self::checkAdmin();

        
        $testList = test::getTestListAdmin();

       
        $questions = Questions::getQuestionsById($id);

        
        if (isset($_POST['submit'])) {
            $options['is_checkbox'] = $_POST['is_checkbox'];
            $options['question_text'] = $_POST['question_text'];
            $options['test_id'] = $_POST['test_id'];
          
            if (Questions::updateQuestionsById($id, $options)) {

            }

            header("Location: /admin/questions");
        }

       
        require_once(ROOT . '/views/admin_questions/update.php');
        return true;
    }
    
    public function actionDelete($id){
        self::checkAdmin();
        
        if(isset($_POST['submit'])){
            Questions::deletequestionsById($id);
            
            header("Location: /admin/questions");
        }
        require_once (ROOT. '/views/admin_questions/delete.php');
        return true;
    }
}