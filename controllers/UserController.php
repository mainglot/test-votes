<?php

class UserController{
    
    public function actionRegister() {
        $name ='';
        $email='';
        $password='';
        $result = false;
        
        if(isset($_POST['submit'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            //todo it had better user password_hash
            $password = md5($_POST['password']);
            
            $errors = false;
            
            if(!User::checkName($name)){
                $errors[] = "Имя должно быть не короче 4 символов"; 
            }
            if(!User::checkEmail($email)){
                $errors[] = "Емаил не соотвутсвует"; 
            }
            if(!User::checkPassword($password)){
                $errors[] = "Пароль должен быть не короче 6 символов"; 
            }
            if(User::checkEmailExists($email)){
                $errors[] = "Такой Емаил уже используется"; 
            }
            
            if($errors == false){
                $result = User::register($name, $email, $password);
            }
            
        }
        
        require_once (ROOT . '/views/user/register.php');
        
        return true;
    }
    
    public function actionLogin() {
        $email = '';
        $password = '';
        
        if(isset($_POST['submit'])){
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            
            $errors = false;
            
            if(!User::checkEmail($email)){
                $errors[] = "Неправельный email"; 
            }
            if(!User::checkPassword($password)){
                $errors[] = "Пароль должен быть не короче 6 символов"; 
            }
            
            $userId = User::checkUserData($email, $password);
            
            if($userId == FALSE){
                $errors[] = 'Неверный пароль или email';
            } else {
                User::auth($userId);
                
                header("Location: /cabinet/");
            }
        }
        require_once (ROOT . '/views/user/login.php');
        
        return true;
            
    }
    public function actionLogout(){

        unset($_SESSION['user_id']);
        header("Location: /");
    }
}

