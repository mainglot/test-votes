<?php

class AdminAnswersController extends AdminBase{
    
    public function actionIndex(){
        self::checkAdmin();
        
        $answersList = Answers::getAnswersList();
        
        require_once (ROOT. '/views/admin_answers/index.php');
        return true;
    }
    
    public function actionCreate(){
        self::checkAdmin();
        
        $questionsList = Questions::getQuestionsListAdmin();
        
        if(isset($_POST['submit'])){
            $options['answer_text'] = $_POST['answer_text'];
            $options['price'] = $_POST['price'];
            $options['que_id'] = $_POST['que_id'];
           
            
            $errors = false;

            //todo you may use !empty() instead of isset + empty
            if(!isset($options['answer_text']) || empty($options['answer_text'])){
                $errors[] = 'Заполните поле answer_text';
            }
           
            if(!isset($options['price']) || empty($options['price'])){
                $errors[] = 'Заполните поле price';
            }
            if(!isset($options['que_id']) || empty($options['que_id'])){
                $errors[] = 'Выберите вопрос';
            }

            //todo bad comparison
            if($errors == false){
                $id = Answers::createAnswers($options);
                
                header("Location: /admin/answers");
            }
            
        }
        //todo bad way to use smth else for example static method
        require_once(ROOT . '/views/admin_answers/create.php');
        return true;
        
    }
    
    public function actionUpdate($id){
       
        self::checkAdmin();

        
        $questionsList = Questions::getQuestionsListAdmin();

       
        $answers = Answers::getAnswersById($id);

        
        if (isset($_POST['submit'])) {
            
            $options['answer_text'] = $_POST['answer_text'];
            $options['price'] = $_POST['price'];
            $options['que_id'] = $_POST['que_id'];
          
            if (Answers::updateAnswersById($id, $options)) {
                //todo remove this condition
            }

            //todo it had better to use another method to redirecting
            header("Location: /admin/answers");
        }

       
        require_once(ROOT . '/views/admin_answers/update.php');
        return true;
    }
    
    public function actionDelete($id){
        self::checkAdmin();
        
        if(isset($_POST['submit'])){
            Answers::deleteAnswersById($id);
            
            header("Location: /admin/answers");
        }
        require_once (ROOT. '/views/admin_answers/delete.php');
        return true;
    }
}