<?php


include_once ROOT . '/models/Questions.php';
include_once ROOT . '/models/Test.php';
include_once ROOT . '/models/Answers.php';
include_once ROOT . '/models/Result.php';

class SiteController
{

    public function actionIndex()
    {


        $tests = array();
        $tests = Test::getTest();

        
       
        require_once(ROOT . '/views/site/index.php');

        return true;
    }

    public function actionIndexsSite($id){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
        }
        $questions = array();
        $questions = Questions::getQuestionsByTest($id);
        
        if(!empty($questions)){
            $question_ids = [];
            foreach($questions as $question){
                $question_ids [] = $question['id'];
            }
            $answers = array();
            $answers = Answers::getAnswersByTest($question_ids);
            
           
        }

        require_once (ROOT . '/views/layouts/questions.php');
        return true;
        
    }
    public function actionIndexRsite(){
        $data = array();
        parse_str(file_get_contents("php://input"), $data);
        $options['test_id'] = $data['test_id'];
        $options['date'] = time();
        unset($data['test_id']);

        $answers = Answers::getAnswersByIds($data);
        $val = 0;
        foreach($answers as $answer){
            $val += $answer['price'];
        }
        $options['result'] = $val;

        //todo hardcoding
        if($val <= 0){
            $result = '<h2>У Вас плохие знания PHP, пройдите тест еще раз и с мануалом.</h2>';
        }elseif($val >= 1 && $val <= 10){
            $result = '<h2>Неплохо, Вы что-то слышали о PHP, но этого не достаточно.</h2>';
        }else{
            $result = '<h2>У Вас хороший результат, продолжайте в том же духе.</h2>';
        }
        Result::insertResult($options);

        require_once (ROOT . '/views/layouts/result.php');
        return true;
    }
  

}
